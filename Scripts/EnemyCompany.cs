﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace STehnology.Scripts
{
    public class EnemyCompany
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        
        // характеризующиеся именем
        public string Name { get; set; } = string.Empty;
        
        // датой поступления
        public DateTime JoinedWork { get; set; } = DateTime.Now;
        
        // группой и базовой ставкой заработной платы.
        public string? IdGroup { get; set; }
        public int Payment { get; set; }

        // У каждого сотрудника может быть начальник
        public string? IdEnemy { get; set; }
        
        // У каждого сотрудника кроме Employee могут быть подчинённые.
        public string? IdParent { get; set; }

        public static void InjectSql()
        {
            SQLite.Instance.AddRangeAsync(new object[]
            {
                new EnemyCompany
                {
                    Name = "Имя1",
                    Payment = 25000,
                    JoinedWork = DateTime.Now.AddYears(-5)
                },
                new EnemyCompany
                {
                    Name = "Имя2",
                    Payment = 40000,
                    JoinedWork = DateTime.Now.AddYears(-3)
                },
                new EnemyCompany
                {
                    Name = "Имя3",
                    Payment = 30000,
                    JoinedWork = DateTime.Now.AddYears(-1)
                }
            });
            SQLite.Instance.SaveChangesAsync();
        }
    }
}